Welcome to Discount Mini Storage of Vero Beach. We have a variety of Climate and Non-Climate Controlled unit sizes to achieve your storage solution. Our on-site management team takes pride in personal service and maintaining an extremely clean, secure and pest free environment.

Address: 1803 90th Avenue, Vero Beach, FL 32966, USA

Phone: 772-569-2723
